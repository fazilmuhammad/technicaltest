import React from 'react'
import { Input } from 'antd';

const SearchBar = ({searchHandler}) => {
  const { Search } = Input;
  return (
   <section className='search center padding'>
        <form className='search-bar' >
        <Search
            className='test'
            placeholder="cari film kesukaanmu"
            enterButton='Search'
            size="large"
            onSearch={value => searchHandler(value)}
        />
        </form>
    </section>
  )
}

export default SearchBar